import * as React from 'react'
import { useRef, useCallback } from 'react'
import { useAppDispatch, useAppSelector } from 'src/store'
import { Element, setCanvasPosition } from '../../store/application'

import '../../scss/components/Canvas.scss'

import Elements from '../Elements'

export interface Props {
  locked: boolean;
  items: Element[];
}

const Canvas: React.FC<Props> = props => {
  const ref = useRef<HTMLDivElement>(null)
  const dispatch = useAppDispatch()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const changeOnCanvasState = useCallback((id: string) => dispatch(setCanvasPosition({ id, remove: false })), [])
  const application = useAppSelector((state) => state.application)
  const locked = props.locked || (application.isDisplayAdded && application.addedOnCanvas < 3)

  let className = 'canvas'
  if (props.items.length) className += ' non-empty'
  if (application.isDragging) className += ' dragging'

  const onDragEnter = () => {
    if (props.items.length === 0) ref?.current?.classList.add('first')
  }

  const onDragLeave = () => {
    if (props.items.length === 0) ref?.current?.classList.remove('first')
  }

  const onDragOver = (e: React.DragEvent) => {
    e.preventDefault()
  }

  const onDrop = (e: React.DragEvent) => {
    e.preventDefault()
    changeOnCanvasState(e.dataTransfer.getData('text'))
  }

  return (
    <div ref={ref} className={className} onDragEnter={onDragEnter} onDragLeave={onDragLeave} onDragOver={onDragOver} onDrop={onDrop}>
      <Elements hidden={false} canvas={true} locked={locked} items={props.items} />
    </div>
  );
}

export default Canvas