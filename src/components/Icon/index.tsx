import * as React from 'react'
import { useEffect, useState } from 'react'
import '../../scss/components/Icon.scss'

export interface Props {
    name: string;
}

const Icon: React.FC<Props> = props => {
    const [SVG, setSVG] = useState('')
    const [isLoaded, setIsLoaded] = useState(false)
    const [isError, setIsError] = useState(false)
  
    useEffect(() => {
        fetch(`/assets/images/${props.name}.svg`)
        .then(res => res.text())
        .then(setSVG)
        .catch(setIsError)
        .then(() => setIsLoaded(true))
    }, [props.name])
  
    return (
        <div 
            className={`svgicon ${isLoaded ? 'loaded' : 'loading'}${isError ? ' error' : ''}`}
            dangerouslySetInnerHTML={{ __html: SVG }}
        />
    )
}

export default Icon