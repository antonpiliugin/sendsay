import * as React from 'react'
import { ElementBlock as Block } from 'src/store/application'
import ElementsRow from '../ElementsRow'

import '../../scss/components/ElementsGroup.scss'

export interface Props {
  shadow: boolean;
  transparent: boolean;
  clickable: boolean;
  rows: Block[][];
  index: number;
}

const ElementsGroup: React.FC<Props> = props => {
  let className = 'elements-group border-radius-6'
  if (props.shadow) className += ' shadow-md'
  if (props.transparent) className += ' transparent'
  if (!props.clickable) className += ' non-clickable'

  return (
    <div className={className}>
        {
            props.rows.map((row, rowIndex) => <ElementsRow clickable={props.clickable} key={`row-${props.index + rowIndex}`} index={props.index + rowIndex} row={row} />)
        }
    </div>
  );
}

export default ElementsGroup