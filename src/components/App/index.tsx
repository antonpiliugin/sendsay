import * as React from 'react'
import { useAppSelector } from 'src/store'
import { MODE } from 'src/store/application'

import '../../scss/components/App.scss'

import Layout from '../Layout'
import Switch from '../Switch'
import Canvas from '../Canvas'
import Elements from '../Elements'

export interface Props {
  children?: React.ReactNode;
}

const App: React.FC<Props> = () => {
  const application = useAppSelector((state) => state.application)
  const onCanvas = application.elements.filter((element) => element.canvasPosition > -1).sort((a, b) => a.canvasPosition - b.canvasPosition)

  return (
    <Layout
      top={<Switch states={application.switch} />}
      left={<Elements canvas={false} locked={false} hidden={application.mode === MODE.RUNTIME} items={application.elements} />}
      right={<Canvas locked={application.mode === MODE.RUNTIME} items={onCanvas} />}
    />
  )
}

export default App
