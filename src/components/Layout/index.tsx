import * as React from 'react';
import '../../scss/components/Layout.scss'


export interface Props {
  top: React.ReactNode;
  left: React.ReactNode;
  right: React.ReactNode;
}

const Layout: React.FC<Props> = props => {
  return (
    <div className='layout'>
      <div className='layout-top'>{props.top}</div>
      <div className='layout-left'>{props.left}</div>
      <div className='layout-right'>{props.right}</div>
    </div>
  );
}

export default Layout