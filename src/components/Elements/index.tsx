import * as React from 'react'
import { useAppSelector } from 'src/store'
import { MODE, Element, ElementBlock as Block } from 'src/store/application'
import ElementsGroup from '../ElementsGroup'
import ElementBlock from '../ElementBlock'
import ElementDisplay from '../ElementDisplay'
import Draggable from '../Draggable'
import Placeholder from '../Placeholder'

import '../../scss/components/Elements.scss'

export interface Props {
  hidden: boolean;
  items: Element[];
  canvas: boolean;
  locked: boolean;
}

const Elements: React.FC<Props> = props => {
  const application = useAppSelector((state) => state.application)
  const { placeholderPosition, isDisplayAdded } = application

  let className = 'elements'
  if (props.hidden) className += ' hidden'

  const elements = props.items.map((item, index, array) => {
    const isFirst = item.canvasPosition > -1 && index === +isDisplayAdded
    const isLast = item.canvasPosition > -1 && index === array.length - 1
    const transparent = !props.canvas && item.canvasPosition > -1
    const draggable = !props.locked && !transparent && !(props.items.length === 1) && !(props.canvas && item.type === 'display')
    const clickable = application.mode === MODE.RUNTIME

    switch (item.type) {
      case 'group':
        return (
          <Draggable first={isFirst} last={isLast} item={item} key={`group-${item.id}`} draggable={draggable}>
            <ElementsGroup shadow={!props.canvas} transparent={transparent} clickable={clickable} index={index} rows={item.rows as Block[][]} />
          </Draggable>
        )
      case 'display':
        return (
          <Draggable first={isFirst} last={isLast} item={item} key={`element-${item.id}`} draggable={draggable}>
            <ElementDisplay shadow={!props.canvas} transparent={transparent} clickable={clickable} />
          </Draggable>
        )
      default:
        return (
          <Draggable first={isFirst} last={isLast} item={item} key={`block-${item.id}`} draggable={draggable}>
            <ElementBlock shadow={!props.canvas} transparent={transparent} clickable={clickable} data={item} />
          </Draggable>
        )
    }
  })

  if (props.canvas && placeholderPosition > -1) elements.splice(placeholderPosition, 0, <Placeholder key='placeholder-0' />)

  return (
    <div className={className}>
      {elements}
    </div>
  );
}

export default Elements