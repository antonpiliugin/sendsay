import * as React from 'react'
import '../../scss/components/Placeholder.scss'

export interface Props {
  
}

const Placeholder: React.FC<Props> = props => {
  return (
    <div className='placeholder'>
        <span className='placeholder-diamond placeholder-diamond-left'></span>
        <span className='placeholder-diamond placeholder-diamond-right'></span>
    </div>
  )
}

export default Placeholder