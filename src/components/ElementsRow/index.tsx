import * as React from 'react'
import { ElementBlock as Block } from 'src/store/application'
import ElementBlock from '../ElementBlock'

import '../../scss/components/ElementsRow.scss'

export interface Props {
  row: Block[];
  clickable: boolean;
  index: number;
}

const ElementsRow: React.FC<Props> = props => {
  return (
    <div className='elements-row border-radius-6'>
        {
            props.row.map((element, index) => <ElementBlock clickable={props.clickable} key={`element-${props.index + index}`} data={element} />)
        }
    </div>
  );
}

export default ElementsRow