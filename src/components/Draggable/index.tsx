import * as React from 'react'
import { useRef, useCallback } from 'react'
import { useAppDispatch, useAppSelector } from 'src/store'
import { setDragging, setPlaceholderPosition, setCanvasPosition, Element, DraggingState, MODE } from '../../store/application'

import '../../scss/components/Draggable.scss'

export interface Props {
    draggable: boolean;
    first: boolean;
    last: boolean;
    item: Element;
    children: React.ReactNode;
}

const Draggable: React.FC<Props> = props => {
    const ref = useRef<HTMLDivElement>(null)
    const application = useAppSelector((state) => state.application)
    const dispatch = useAppDispatch()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    const removeFromCanvas = useCallback(() => dispatch(setCanvasPosition({ id: props.item.id, remove: true })), [])
    // eslint-disable-next-line react-hooks/exhaustive-deps
    const changeDraggingState = useCallback((payload: DraggingState) => dispatch(setDragging(payload)), [])
    // eslint-disable-next-line react-hooks/exhaustive-deps
    const changePlaceholerPosition = useCallback((position: number) => dispatch(setPlaceholderPosition(position)), [])

    const onDragStart = (e: React.DragEvent) => {
        if (!props.draggable) return e.preventDefault()

        changeDraggingState({
            dragging: true,
            first: props.first,
            last: props.last,
            isDisplay: props.item.type === 'display',
            fromCanvas: props.item.canvasPosition > -1
        })
        e.dataTransfer.effectAllowed = 'copyMove'
        e.dataTransfer.dropEffect = 'copy'
        e.dataTransfer.clearData()
        e.dataTransfer.setData('text/plain', props.item.id)
        ref?.current?.classList.add('dragging')
    }

    const onDragEnd = () => {
        changeDraggingState({
            dragging: false,
            first: props.first,
            last: props.last,
            isDisplay: props.item.type === 'display',
            fromCanvas: props.item.canvasPosition > -1
        })
        ref?.current?.classList.remove('dragging')
    }

    const onDragOver = (e: React.DragEvent): any => {
        if (props.item.canvasPosition > -1) {
            if (props.item.type === 'display') return changePlaceholerPosition(1)

            const rect = ref?.current?.getBoundingClientRect()
            if (!rect) return

            const after = e.pageY >= (rect.top + (rect.height / 2))
            const newPosition = after ? props.item.canvasPosition + 1 : props.item.canvasPosition - 1
            changePlaceholerPosition(newPosition)
        }
    }

    const onDoubleClick = (e: React.MouseEvent) => {
        if (application.mode === MODE.RUNTIME) return e.preventDefault()

        if (props.item.canvasPosition !== -1) removeFromCanvas()
    }

    let className = 'draggable'
    if (!props.draggable && application.mode === MODE.CONSTRUCTOR) className += ' non-draggable'

    return (
        <div ref={ref} className={className} onDoubleClick={onDoubleClick} onDragStart={onDragStart} onDragEnd={onDragEnd} onDragOver={onDragOver} draggable={props.draggable}>
            {props.children}
        </div>
    )
}

export default Draggable
