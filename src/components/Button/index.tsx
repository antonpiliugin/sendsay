import * as React from 'react'
import '../../scss/components/Button.scss'

export interface Props {
  children: React.ReactNode
}

const Button: React.FC<Props> = props => {
  return (
    <div className='button border-radius-6'>
      {props.children}
    </div>
  );
}

export default Button