import * as React from 'react'
import { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { ModeSwitch, switchState } from '../../store/application'

import Icon from '../Icon'

export interface Props {
  active: boolean;
  index: number;
  state: ModeSwitch;
}

const SwitchState: React.FC<Props> = props => {
  const dispatch = useDispatch()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const onClick = useCallback((index: number) => dispatch(switchState({ index })), [])

  let className = 'switch-state border-radius-6'
  if (props.active) className += ' active'

  return (
    <div className={className} onClick={() => !props.active && onClick(props.index)}>
      <Icon name={props.state.icon} />
      {props.state.text}
    </div>
  );
}

export default SwitchState