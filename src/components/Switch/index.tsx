import * as React from 'react'
import { useEffect, useRef, useState } from 'react'
import { useAppDispatch, useAppSelector } from 'src/store'
import { reset } from '../../store/calculator'
import SwitchState from './SwitchState'
import { ModeSwitch } from 'src/store/application'

import '../../scss/components/Switch.scss'

export interface Props {
  states: ModeSwitch[]
}

const Switch: React.FC<Props> = props => {
  const dispatch = useAppDispatch()
  const application = useAppSelector((state) => state.application)
  const ref = useRef<HTMLDivElement>(null)
  const [activeLeft, setActiveLeft] = useState('0px')
  const [activeWidth, setActiveWidth] = useState('0px')

  const activeBgStyle = {
    left: activeLeft,
    width: activeWidth
  }

  useEffect(() => {
    dispatch(reset())
    if (ref.current) {
      const activeElement = ref.current?.children[application.mode + 1] as HTMLDivElement
      if (activeElement) {
        setActiveLeft(`${activeElement.offsetLeft}px`)
        setActiveWidth(`${activeElement.offsetWidth}px`)
      }
    }
    
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [application.mode]);

  return (
    <div ref={ref} className='switch border-radius-6'>
      <div className='switch-active-bg' style={activeBgStyle}></div>
      {props.states.map((state, index) => <SwitchState key={`switch-${index}`} active={application.mode === index} index={index} state={state} />)}
    </div>
  );
}

export default Switch