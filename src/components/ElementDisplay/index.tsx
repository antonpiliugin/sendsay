import * as React from 'react'
import { useAppSelector } from 'src/store'

import '../../scss/components/ElementDisplay.scss'

export interface Props {
  shadow: boolean;
  transparent: boolean;
  clickable: boolean;
}

const ElementDisplay: React.FC<Props> = props => {
  const text = useAppSelector((state) => state.calculator.screen)

  let className = 'element-display border-radius-6'
  const textStyle = {
    fontSize: `${36 - text.length}px`
  }
  if (props.shadow) className += ' shadow-md'
  if (props.transparent) className += ' transparent'
  if (!props.clickable) className += ' non-clickable'

  return (
    <div className={className}>
        <div className='element-display-text border-radius-6' style={textStyle}>
            {text}
        </div>
    </div>
  )
}

export default ElementDisplay