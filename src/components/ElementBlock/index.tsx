import * as React from 'react'
import { useCallback } from 'react'
import { useAppDispatch, useAppSelector } from 'src/store'
import { ElementBlock as ElementData } from 'src/store/application'
import { CalculatorInputData, update } from 'src/store/calculator'

import '../../scss/components/ElementBlock.scss'

import Button from '../Button'

export interface Props {
  data: ElementData;
  clickable: boolean;
  shadow?: boolean;
  transparent?: boolean;
}

const ElementBlock: React.FC<Props> = props => {
  const dispatch = useAppDispatch()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const updateCallback = useCallback((payload: CalculatorInputData) => dispatch(update(payload)), [])
  const buttonEnabled = useAppSelector((state) => state.calculator.resultButtonEnabled)
  let text: string | React.ReactNode = props.data.text || props.data.value
  const properties: React.ButtonHTMLAttributes<HTMLButtonElement> = {
    className: 'element-block border-radius-6'
  }

  if (props.data.grow) properties.className += ' grow'
  if (props.shadow) properties.className += ' shadow-md'
  if (props.transparent) properties.className += ' transparent'
  if (!props.clickable) properties.className += ' non-clickable'

  const payload: CalculatorInputData = {
    isOperator: props.data.type !== 'operand',
    value: props.data.value,
    text: props.data.text
  }

  switch (props.data.type) {
    case 'button':
      properties.className += ' container shadow-md'
      properties.disabled = !buttonEnabled
      properties.onClick = () => updateCallback(payload)
      text = <Button>{text}</Button>
      break
    default:
      properties.onClick = () => updateCallback(payload)
      break
  }

  return (
    <button {...properties}>
      {text}
    </button>
  )
}

export default ElementBlock