import { createSlice } from '@reduxjs/toolkit'

export interface CalculatorState {
  expression: string;
  operands: number[];
  operator: string | null;
  screen: string;
  clearScreen: boolean;
  resultButtonEnabled: boolean;
  used: boolean;
}

export interface CalculatorInputData {
  isOperator: boolean;
  value?: string;
  text?: string;
}

const calculator = createSlice({
    name: 'calculator',
    initialState: {
      expression: '0',
      operands: [],
      operator: null,
      screen: '0',
      clearScreen: false,
      resultButtonEnabled: false,
      used: false
    } as CalculatorState,
    reducers: {
      update: (state, { payload }) => {
        if (!payload.isOperator) {
            const isComma = payload.value === '.'
            if ((!state.expression || (state.expression && state.expression[state.expression.length - 1] === payload.value)) && isComma) return
            
            if ((state.clearScreen || !state.used) && !isComma) {
              state.screen = ''
              state.clearScreen = false
              state.expression = ''
            } else if (isComma) {
              state.clearScreen = false
            }

            if (state.expression.length >= 17 || state.screen.length >= 17) return

            state.expression += payload.value
            state.screen += payload.text || payload.value
            state.used = true
        } else {
            if (!state.used) return

            const hasOperator = state.operator !== null
            const isMathOperator = payload.value !== '='
            if (!hasOperator && isMathOperator) state.operator = payload.value

            const number = Number(state.expression || state.screen)
            if (isFinite(number)) state.operands.push(number)

            if (state.operands.length === 2) {
              let result: number = Infinity
              switch (state.operator) {
                case '+':
                  result = state.operands[0] + state.operands[1]
                  break
                case '-':
                  result = state.operands[0] - state.operands[1]
                  break
                case '/':
                  result = state.operands[0] / state.operands[1]
                  break
                case '*':
                  result = state.operands[0] * state.operands[1]
                  break
                default:
                  break
              }
              
              const isFiniteResult = isFinite(result)
              state.expression = !isMathOperator ? result.toString() : ''
              state.operands = isFiniteResult && isMathOperator ? [result] : []
              state.operator = null
              state.screen = isFiniteResult ? result.toString().replace('.', ',') : 'Не определено'
            }

            if (hasOperator && isMathOperator) state.operator = payload.value
      
            state.clearScreen = true
        }

        state.resultButtonEnabled = state.operands.length > 0 && state.expression.length > 0
      },
      reset: (state) => {
        state.expression = '0'
        state.operands = []
        state.operator = null
        state.screen = '0'
        state.clearScreen = false
        state.resultButtonEnabled = false
        state.used = false
      }
    }
})

export const { update, reset } = calculator.actions
export default calculator.reducer