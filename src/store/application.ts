import { createSlice } from '@reduxjs/toolkit'

function clamp(value: number, min: number, max: number): number {
  return Math.min(Math.max(value, min), max)
}

export enum MODE {
  RUNTIME = 0,
  CONSTRUCTOR = 1
}

export interface ModeSwitch {
  icon: string;
  text: string;
}

export interface ElementBlock {
  type: string;
  value?: string;
  text?: string;
  grow?: boolean;
}

export interface Element {
  id: string;
  type: string;
  canvasPosition: number;
  rows?: ElementBlock[][]
}

export interface ApplicationState {
  mode: MODE,
  isDragging: boolean,
  isDraggingFromCanvas: boolean,
  isDraggingFirst: boolean,
  isDraggingLast: boolean,
  isDraggingDisplay: boolean,
  isDisplayAdded: boolean,
  switch: ModeSwitch[],
  elements: Element[],
  addedOnCanvas: number,
  placeholderPosition: number
}

export interface DraggingState {
  dragging: boolean;
  first: boolean;
  last: boolean;
  isDisplay: boolean;
  fromCanvas: boolean;
}

const application = createSlice({
    name: 'application',
    initialState: {
        mode: MODE.CONSTRUCTOR,
        isDragging: false,
        isDraggingFromCanvas: false,
        isDraggingFirst: false,
        isDraggingLast: false,
        isDraggingDisplay: false,
        isDisplayAdded: false,
        switch: [
            {
                icon: 'eye',
                text: 'Runtime'
            },
            {
                icon: 'selector',
                text: 'Constructor'
            }
        ],
        elements: [
          {
              id: '0',
              type: 'display',
              canvasPosition: -1
          },
          {
              id: '1',
              type: 'group',
              rows: [
                  [
                      { type: 'operator', value: '/' },
                      { type: 'operator', value: '*' },
                      { type: 'operator', value: '-' },
                      { type: 'operator', value: '+' }
                  ]
              ],
              canvasPosition: -1
          },
          {
              id: '2',
              type: 'group',
              rows: [
                  [
                      { type: 'operand', value: 7 },
                      { type: 'operand', value: 8 },
                      { type: 'operand', value: 9 }
                  ],
                  [
                      { type: 'operand', value: 4 },
                      { type: 'operand', value: 5 },
                      { type: 'operand', value: 6 }
                  ],
                  [
                      { type: 'operand', value: 1 },
                      { type: 'operand', value: 2 },
                      { type: 'operand', value: 3 }
                  ],
                  [
                      { type: 'operand', value: 0, grow: true },
                      { type: 'operand', text: ',', value: '.' }
                  ]
              ],
              canvasPosition: -1
          },
          {
              id: '3',
              type: 'button',
              value: '=',
              canvasPosition: -1
          }
        ],
        addedOnCanvas: 0,
        placeholderPosition: -1
    } as ApplicationState,
    reducers: {
      switchState: (state, { payload }) => {
        state.mode = payload.index
      },
      setDragging: (state, { payload }) => {
        if (payload.dragging === state.isDragging) return

        state.isDragging = payload.dragging
        state.isDraggingFirst = payload.first
        state.isDraggingLast = payload.last
        state.isDraggingDisplay = payload.isDisplay
        state.isDraggingFromCanvas = payload.fromCanvas

        if (state.addedOnCanvas > 0) {
          state.placeholderPosition = payload.isDisplay ? 0 : state.addedOnCanvas
        } else {
          state.placeholderPosition = -1
        }
      },
      setCanvasPosition: (state, { payload }) => {
        state.isDragging = false

        const elementIndex = state.elements.findIndex((el) => el.id === payload.id)
        if (elementIndex < 0) return

        const element = state.elements[elementIndex]
        
        const previousPosition = element.canvasPosition
        
        if (element.type === 'display') {
          state.isDisplayAdded = !payload.remove
          element.canvasPosition = payload.remove ? -1 : 0
        } else {
          element.canvasPosition = payload.remove ? -1 : (state.addedOnCanvas > 0 ? state.placeholderPosition : 0)
        }

        const movedDown = payload.remove || (previousPosition < element.canvasPosition && state.isDraggingFromCanvas)
        const firstIndex = movedDown ? previousPosition : element.canvasPosition
        const lastIndex = movedDown && !payload.remove ? element.canvasPosition : state.isDraggingFromCanvas ? previousPosition : state.elements.length

        for (let i = 0; i < state.elements.length; i++) {
          if (i !== elementIndex && state.elements[i].canvasPosition >= firstIndex && state.elements[i].canvasPosition <= lastIndex) {
            state.elements[i].canvasPosition += movedDown ? -1 : 1
          }
        }

        if (payload.remove) {
          state.addedOnCanvas--
        } else if (!state.isDraggingFromCanvas) {
          state.addedOnCanvas++
        }
      },
      setPlaceholderPosition: (state, { payload }) => {
        if (state.isDraggingDisplay) return

        payload = clamp(payload, +state.isDraggingFirst + +state.isDisplayAdded, (state.addedOnCanvas - +state.isDraggingLast) + +state.isDisplayAdded)

        if (state.placeholderPosition === payload) return

        state.placeholderPosition = payload
      }
    }
})

export const { switchState, setDragging, setCanvasPosition, setPlaceholderPosition } = application.actions
export default application.reducer